#!/usr/bin/env bash

Pre(){
sudo groupadd docker
sudo usermod -aG docker $USER
}

# Clone the main repo
git pull

# Setup the other repos
git clone --depth 1 https://gitlab.com/Minds/front front
git clone --depth 1 https://gitlab.com/Minds/engine engine



# full cluster
fullCluster(){
    #- runners
    sudo sed -i 's/#  - mongo/  - mongo/' docker-compose.yml
    sudo sed -i 's/#  - elasticsearch/  - elasticsearch/' docker-compose.yml
    sudo sed -i 's/#  - rabbitmq/  - rabbitmq/' docker-compose.yml

    #- php-fpm
    sudo sed -i 's/#- mongo/  - mongo/' docker-compose.yml
    sudo sed -i 's/#- elasticsearch/- elasticsearch/' docker-compose.yml
}